<section id="about" class="section">
    <!-- section overlay -->
    <div class="overlay-about">
        <div class="container">
        <!-- Section Title -->
          <div class="section-title" >
              <h2><i class="fa fa-code" aria-hidden="true"></i> About me</h2>
              <span class="border"></span>
          </div>
        <!-- Section Title End -->
          <div class="row">
            <div class="col-md-6">
              <a href="/images/profile.jpg">
                <img class="img-about alignnone wp-image-102 size-full" src="/images/profile.jpg" alt="profile" width="600" height="403"/>
              </a>
            </div>
            <div class="col-md-6">
              <h2>A FEW WORDS ABOUT ME.</h2>
              <p>I'm <strong>Christopher Ross dela Cruz</strong>, Front-end Web Developer, working in web development industry. If you have a project that needs some creative injection then that's where I come in!</p>
              <p>My job is to build your website so that it is functional and user-friendly but at the same time attractive. Moreover, I add personal touch to your product and make sure that is eye-catching and easy to use. My aim is to bring across your message and identity in the most creative way.</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
    </div>
    <!-- section overlay end -->
</section>
<!-- end of sections -->

<style>
    #separator_1{ background-image:url('/images/41.jpg'); }
    .parallax-overlay-1{ background-color:#000;   opacity: 0.8;}
</style>

 <style>
   #separator_3{ background-image:url('/images/11.jpg');  }
   .parallax-overlay-3{ background-color:#071013; opacity: 0.8; }
 </style>
 <div id="separator_3" class="parallax">
   <div class="parallax-overlay-3">
     <div class="container">
       <h1>"Design is hard work. Striving for design excellence is even harder."</h1>
         -Archie Boston
     </div>
   </div>
 </div>
