
<!-- section -->
  <section id="resume" class="section">
    <!-- section overlay -->
    <div class="overlay-resume">
      <div class="container">
        <!-- Section Title -->
          <div class="section-title" >
            <h2><i class="fa fa-code" aria-hidden="true"></i> My Resume</h2>
            <span class="border"></span>
          </div>
        <!-- Section Title End -->
        <!-- Experience Timeline -->
        <div class="row">
          <div class="col-md-6">
            <div class="timeline">
              <!-- Item Head -->
                <div class="timeline-item">
                  <div class="timeline-head">
                    <span class="fa fa-lightbulb-o"><i></i></span>
                  </div>
                  <div class="timeline-arrow"><i></i></div>
                  <div class="timeline-head-content">
                      <h4>Work Experience</h4>
                  </div>
                </div>
              <!-- /Item Head end -->
              <!-- Timeline item -->
              <div class="timeline-item">
                <div class="timeline-item-date"> 2015 - now</div><!-- date -->
                  <div class="timeline-item-trigger">
                    <span class="fa fa-minus" data-toggle="collapse" data-target="#skyrockit"><i></i></span>
                  </div>
                  <div class="timeline-arrow"><i></i></div>
                  <!-- Timeline main content -->
                  <div class="timeline-item-content">
                    <h4 class="timeline-item-title" data-toggle="collapse" data-target="#skyrockit">SkyRockIT</h4>
                    <div class="collapse in" id="skyrockit">
                      <p><small class="muted">Cebu, Philippines | 2015 - Present</small></p>
                      <p>
                        <ul class="list-unstyled">
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> Lorem</li>
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                        </ul>
                      </p>
                    </div>
                  </div>
                  <!-- /end of timeline content -->
              </div>
              <!-- /end of timeline item -->
              <!-- Timeline item -->
              <div class="timeline-item">
                <div class="timeline-item-date">2014 - 2015</div><!-- date -->
                  <div class="timeline-item-trigger">
                    <span class="fa fa-plus" data-toggle="collapse" data-target="#webcada"><i></i></span>
                  </div>
                  <div class="timeline-arrow"><i></i></div>
                  <!-- Timeline main content -->
                  <div class="timeline-item-content">
                    <h4 class="timeline-item-title" data-toggle="collapse" data-target="#webcada">Webcada</h4>
                    <div class="collapse" id="webcada">
                      <p><small class="muted">Singapore | 2014 - 2015</small></p>
                      <p>
                        <ul class="list-unstyled">
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> Lorem</li>
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                          <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                        </ul>
                      </p>
                    </div>
                  </div>
                  <!-- /end of timeline content -->
                </div>
                <!-- /end of timeline item -->
                <!-- Timeline item -->
                <div class="timeline-item">
                  <div class="timeline-item-date">2013 - 2014</div><!-- date -->
                    <div class="timeline-item-trigger">
                      <span class="fa fa-plus" data-toggle="collapse" data-target="#tms"><i></i></span>
                    </div>
                    <div class="timeline-arrow"><i></i></div>
                    <!-- Timeline main content -->
                    <div class="timeline-item-content">
                      <h4 class="timeline-item-title" data-toggle="collapse" data-target="#tms">TheTMSway</h4>
                      <div class="collapse" id="tms">
                        <p><small class="muted">Cebu, Philippines | 2013 - 2014</small></p>
                        <p>
                          <ul class="list-unstyled">
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> Lorem</li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                          </ul>
                        </p>
                      </div>
                    </div>
                    <!-- /end of timeline content -->
                </div>
                <!-- /end of timeline item -->
                <!-- Timeline item -->
                <div class="timeline-item">
                  <div class="timeline-item-date">2012 - 2013</div><!-- date -->
                    <div class="timeline-item-trigger">
                      <span class="fa fa-plus" data-toggle="collapse" data-target="#smarttraffic"><i></i></span>
                    </div>
                    <div class="timeline-arrow"><i></i></div>
                    <!-- Timeline main content -->
                    <div class="timeline-item-content">
                      <h4 class="timeline-item-title" data-toggle="collapse" data-target="#smarttraffic">Smart Traffic Ltd</h4>
                      <div class="collapse" id="smarttraffic">
                        <p><small class="muted">Cebu, Philippines | 2012 - 2013</small></p>
                        <p>
                          <ul class="list-unstyled">
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> Lorem</li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                          </ul>
                        </p>
                      </div>
                    </div>
                    <!-- /end of timeline content -->
                </div>
                <!-- /end of timeline item -->
                <!-- Timeline item -->
                <div class="timeline-item">
                  <div class="timeline-item-date">2011 - 2012</div><!-- date -->
                    <div class="timeline-item-trigger">
                      <span class="fa fa-plus" data-toggle="collapse" data-target="#trimorph"><i></i></span>
                    </div>
                    <div class="timeline-arrow"><i></i></div>
                    <!-- Timeline main content -->
                    <div class="timeline-item-content">
                      <h4 class="timeline-item-title" data-toggle="collapse" data-target="#trimorph">Trimorph</h4>
                      <div class="collapse" id="trimorph">
                        <p><small class="muted">Cebu, Philippines | 2011 - 2012</small></p>
                        <p>
                          <ul class="list-unstyled">
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> Lorem</li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                            <li><i class="list-fa fa fa-terminal" aria-hidden="true"></i> </li>
                          </ul>
                        </p>
                      </div>
                    </div>
                    <!-- /end of timeline content -->
                </div>
                <!-- /end of timeline item -->
            </div>
          </div>

          <div class="col-md-6">
            <div class="timeline">
            <!-- Item Head -->
              <div class="timeline-item">
                <div class="timeline-head">
                  <span class="li_study"><i></i></span>
                </div>
                <div class="timeline-arrow"><i></i></div>
                <div class="timeline-head-content">
                    <h4>Education Experience</h4>
                </div>
              </div>
              <!-- /Item Head end -->
              <!-- Timeline item -->
              <div class="timeline-item">
                <div class="timeline-item-date"> 2007 - 2011</div><!-- date -->
                  <div class="timeline-item-trigger">
                    <span class="fa fa-minus" data-toggle="collapse" data-target="#college-in-computer-science"><i></i></span>
                  </div>
                  <div class="timeline-arrow"><i></i></div>
                  <!-- Timeline main content -->
                  <div class="timeline-item-content">
                    <h4 class="timeline-item-title" data-toggle="collapse" data-target="#college-in-computer-science">University of Cebu</h4>
                    <div class="collapse in" id="college-in-computer-science">
                      <p><small class="muted">Cebu, Philippines | 2007 - 2011</small></p>
                      <p>
                        <i class="fa fa-terminal" aria-hidden="true"></i> Bacherlor of Science in Information Technology
                      </p>
                    </div>
                  </div>
                  <!-- /end of timeline content -->
              </div>
              <!-- /end of timeline item -->

          </div>
        </div>
      </div>
        <!-- /Education Timeline End -->

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        </div>
    </div>
    <!-- section overlay end -->
</section>
<!-- end of sections -->
