  <!-- Footer -->
  	<footer>
  	  <div class="container">
  		  <!-- widgets -->
  		  <div class="row"></div>
  			<!-- widgets end -->
  	    <div class="copyright">©2016 made with love by <a href="#">atchoi.delacruz</a></div>
  	  </div>
    </footer>
  <!-- Footer end -->
    <style>
      .parallax-overlay { background: rgba(0,0,0,.6); height: 100%; }
    </style>

    <a class="scroll-top" href="#" title="Scroll to top"><i class="fa fa-arrow-up"></i></a>
    <script src="{{ mix('js/plugins.js') }}"></script>
