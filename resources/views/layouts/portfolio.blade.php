
  <style>  .parallax-overlay-7{opacity: 0.8;} </style>
  <!--div id="separator_7" class="parallax">
    <div class="parallax-overlay-7">
      <div class="container"></div>
    </div>
  </div-->

<section id="portfolio" class="section">
    <!-- section overlay -->
  <div class="overlay-portfolio">
    <div class="container">
      <!-- Section Title -->
      <div class="section-title" >
        <h2><i class="fa fa-code" aria-hidden="true"></i> Portfolio</h2>
        <span class="border"></span>
      </div>
      <!-- Section Title End -->
      <div class="portfolio-start"></div>
      <!-- Project Details-->
      <div id="project-details-box">
        <div id="project-data"></div>
      </div>
          <!--/Project Details end-->
      <!-- #options -->
      <div id="options" class="clearfix">
        <ul id="filters" class="option-set clearfix" data-option-key="filter">
          <li><a href="#filter" data-option-value="*" class="selected">all</a></li>
          <li><a href="#filter" data-option-value=".branding">Wordpress</a></li>
          <li><a href="#filter" data-option-value=".illustration">Drupal</a></li>
          <li><a href="#filter" data-option-value=".logodesign">Landing Pages</a></li>
          <li><a href="#filter" data-option-value=".photography">Laravel</a></li>
        </ul>
        <!-- #options end-->
      </div>
      <!-- Portfolio container -->
      <div class="portfolio-container">
        <!-- Portfolio Item-->
        @for ($i = 1; $i < 7; $i++)
          <div class="portfolio-item mt photography">
            <a class="project-link" href="#" title="Project Title">
              <div class="portfolio-image">
                  <img src="/images/{{ 'home-' . $i }}.jpg" alt="Project Title"/>
              </div>
              <div class="item-overlay">
                <div class="item-info">
                  <div class="zoom-icon"></div>
                  <h4 class="item-name">Project Title</h4>
                  <p class="item-type">photography</p>
                </div>
              </div>
             </a>
          </div>

        @endfor
        <!--/Portfolio Item -->
       </div>
      <!-- Portfolio container end-->
      </div>
    </div>
  <!-- section overlay end -->
</section>
<!-- end of sections -->
<!-- separator -->
