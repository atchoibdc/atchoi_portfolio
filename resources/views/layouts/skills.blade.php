<!-- section -->
<section id="skills" class="section">
    <!-- section overlay -->
    <div class="overlay-skills">
        <div class="container">
          <!-- Section Title -->
          <div class="section-title" >
              <h2><i class="fa fa-code" aria-hidden="true"></i> I am good at</h2>
              <span class="border"></span>
          </div>
          <!-- Section Title End -->
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <ul class="skill">
                <li>
                    <div class="skill-wrap"><span data-width="80" style="background:#00A676;"><strong>Wordpress 80%</strong></span></div>
                </li>
                <li>
                    <div class="skill-wrap"><span data-width="50" style="background:#224870;"><strong>Laravel 50%</strong></span></div>
                </li>
                <li>
                    <div class="skill-wrap"><span data-width="60" style="background:#FF8B53;"><strong>Angular JS 60%</strong></span></div>
                </li>
              </ul>
            </div>
            <div class="col-sm-6 col-xs-12">
              <ul class="skill">
                <li>
                    <div class="skill-wrap"><span data-width="100" style="background:#DE3C4B;"><strong>HTML5 100%</strong></span></div>
                </li>
                <li>
                    <div class="skill-wrap"><span data-width="90" style="background:#228CDB;"><strong>CSS3 90%</strong></span></div>
                </li>
                <li>
                    <div class="skill-wrap"><span data-width="70" style="background:#F4D761;"><strong>JQuery 70%</strong></span></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </div>
    <!-- section overlay end -->
  </section>
<!-- end of sections -->

<!-- separator -->
<style>
  #separator_4{ background-image:url('/images/21.jpg'); }
  .parallax-overlay-4{background-color: #616163; opacity: 0.8; }
</style>
<div id="separator_4" class="parallax">
  <div class="parallax-overlay-4">
    <div class="container">
      <!--Title-->
      <div class="title">
          <h1>Work Process</h1>
          <span class="border"></span>
      </div>
      <!--Title-->
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
            <div class="work_process-box">
              <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
              <h4>IDEA</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
          <div class="work_process-box">
              <i class="fa fa-puzzle-piece"></i>
              <h4>CONCEPT</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
            <div class="work_process-box">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              <h4>DESIGN</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
            <div class="work_process-box">
              <i class="fa fa-code"></i>
              <h4>DEVELOP</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
            <div class="work_process-box">
              <i class="fa fa-bug"></i>
              <h4>TEST</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
          <!-- work_process -->
          <div class="work_process">
            <div class="work_process-box">
              <i class="fa fa-rocket"></i>
              <h4>LAUNCH</h4>
            </div>
          </div>
          <!-- work_process -->
        </div>
      </div>
    </div>
  </div>
 </div>
