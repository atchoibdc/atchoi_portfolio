
<section id="home" class="parallax" style="background-image: url('/images/bg31.jpg');">
  <div class="parallax-overlay">
    <div class="container">
      <div class="col-md-12">
        <div class="avater" style="background-image: url('/images/me.jpg');background-size: cover;"></div>
        <div class="info">
          <h1>@choi.dela.Cruz</h1>
          <h2>Web Developer <span style="color:#f1b663">.</span> Photographer <span style="color:#f1b663">.</span> Wanderer</h2>
        </div>
        {{-- <div class="gotosection">
          <a href="#about"><i class="fa fa-angle-double-down "></i></a>
        </div> --}}
      </div>
    </div>
  </div>
</section>
