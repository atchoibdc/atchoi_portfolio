<!-- Navigation -->
<div class="main-nav">

    <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <!-- navigation -->
            <ul id="navs" class="nav navbar-nav">
              <li class="menu-item"><a href="#home" class="colapse-menu1">Home</a></li>
              <li class="menu-item"><a href="#about" class="colapse-menu1">About</a></li>
              <li class="menu-item"><a href="#skills" class="colapse-menu1">Skills</a></li>
              {{-- <li class="menu-item"><a href="#resume" class="colapse-menu1">Resume</a></li> --}}
              <li class="menu-item"><a href="#portfolio" class="colapse-menu1">Portfolio</a></li>
              <li class="menu-item"><a href="#contact" class="colapse-menu1">Contact</a></li>
            </ul>
        <!-- navigation end -->
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

</div>
<!-- End of Navigation -->
