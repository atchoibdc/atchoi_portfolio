      <!-- separator -->
      <style>
          #separator_8{background-image:url('/images/11.jpg');}
          .parallax-overlay-8{background-color: #000000;opacity: 0.8;}
      </style>
      <div id="separator_8" class="parallax">
        <div class="parallax-overlay-8">
          <div class="container">
            <h1>Artists themselves are not confined, but their output is.</h1> -Robert Smithson
          </div>
        </div>
      </div>
      <!-- end of separator -->
      <!-- section -->
      <section id="contact" class="section">
        <!-- section overlay -->
        <div class="overlay-contact">
          <div class="container">
            <!-- Section Title -->
            <div class="section-title" >
                <h2><i class="fa fa-code" aria-hidden="true"></i> Contact me</h2>
                <span class="border"></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco . </p>
            </div>
            <!-- Section Title End -->
  					<div class="row">
  						<div class="col-md-6">
  							<h2>LETS GET IN TOUCH</h2>
  							<ul class="get_in">
  								<li><i class="fa fa-envelope-o"></i><p>atchoi.delacruz@gmail.com</p></li>
  								<li><i class="fa fa-phone"></i><p>+032-932-255-0547</p></li>
  								<li><i class="fa fa-map-marker"></i><p>30-A B. Aranas st., Cebu City, Cebu, Philippines</p></li>
  							</ul>
             	  <!-- social Icons -->
                <ul class="social">
  		            <li>
                    <a class="facebook" target="_blank" href="https://www.fb.com/atchoibdc/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
  		            <li>
                    <a class="instagram" target="_blank" href="https://www.instagram.com/atchoilakwatchero/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </li>
  		            <li>
                    <a class="linkedin" target="_blank" href="https://www.linkedin.com/in/christopher-ross-dela-cruz-87b16382/">
                      <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                  </li>
  		            <li>
                    <a class="flickr" target="_blank" href="https://www.flickr.com/photos/atchoi_d_cruz/">
                      <i class="fa fa-flickr" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
                <!-- social Icons end -->
  						</div>
  						<div class="col-md-6">
  						  <h2>DROP ME A LINE</h2>
  							<!-- contact from -->
  							<form class="form" id="form" method="post" action="mail/contact.php">
  									<input type="text" name="name"  placeholder="Name" required/>
  									<input type="text" name="email" placeholder="Email" required/>
  									<textarea name="message" placeholder="Message" rows="8" ></textarea>
  									<input type="submit" class="mt-button medium outline"  id="submit" value="Send Message">
  							</form>
  							<span class="sending">
  								sending...
  							</span>
  							<div class="mess center"></div>
  							<!-- contact from end-->
              </div>
  					</div>
          </div>
        </div>
        <!-- section overlay end -->
      </section>
      <!-- end of sections -->
