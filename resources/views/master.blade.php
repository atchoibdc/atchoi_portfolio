<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atchoi dela Cruz - Portfolio Site</title>

    <!-- Fonts -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Montserrat:400,700|' type='text/css' media='all' />

    <!-- Stylesheets -->
    <link rel='stylesheet' href='/fonts/stylesheet.css' type='text/css' media='all' />
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">

    <!-- Core JS -->
    <script src="{{ mix('js/core.js') }}"></script>
    {{-- <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;'></script> --}}

  </head>
  <body>
    <div class="mask">
      <div class="bubblingG">
        <span id="bubblingG_1"></span>
        <span id="bubblingG_2"></span>
        <span id="bubblingG_3"></span>
      </div>
    </div>

    <!-- START PAGE WRAPPER -->
    <div id="page-wrapper">

      @include('layouts.profile')

      @include('layouts.nav')

      @include('layouts.about')

      @include('layouts.skills')

      @include('layouts.resume')

      @include('layouts.portfolio')

      @include('layouts.contact')

      @include('layouts.footer')
    </div>
    <!-- END PAGE WRAPPER -->
  </body>
</html>
