const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/sass/plugins/bootstrap.scss',
    'resources/assets/sass/plugins/shortcodes.scss',
    'resources/assets/sass/plugins/bootstrap.min.scss',
    'resources/assets/sass/font-awesome.min.scss',
    'resources/assets/sass/plugins/animate.scss',
    'resources/assets/sass/plugins/owl.carousel.scss',
    'resources/assets/sass/plugins/owl.theme.scss',
    'resources/assets/sass/plugins/responsive.scss',
    'resources/assets/sass/plugins/linecons.scss',
		'resources/assets/sass/plugins/color-gray.scss',
    'resources/assets/sass/styles.scss'
], 'public/css/all.css');

mix.scripts([
    'resources/assets/js/plugins/jquery.js',
    'resources/assets/js/plugins/jquery-migrate.min.js',
    'resources/assets/js/plugins/bootstrap.js',
    'resources/assets/js/plugins/init.js'
], 'public/js/core.js');

mix.scripts([
    'resources/assets/js/plugins/bootstrap.min.js',
    'resources/assets/js/plugins/jquery.nav.js',
    'resources/assets/js/plugins/jquery.scrollTo.js',
    'resources/assets/js/plugins/scripts.js',
    'resources/assets/js/plugins/appear.js',
    'resources/assets/js/plugins/jquery.form.js',
    'resources/assets/js/plugins/jquery.ufvalidator-1.0.5.js',
    'resources/assets/js/plugins/jquery.isotope.min.js',
    'resources/assets/js/plugins/jquery.easing-1.3.pack.js',
    'resources/assets/js/plugins/owl.carousel.js',
    'resources/assets/js/plugins/jquery.parallax-1.1.3.js',
    'resources/assets/js/plugins/skrollr.js',
    'resources/assets/js/plugins/jquery.tweetscroll.js',
    'resources/assets/js/plugins/jquery.nicescroll.min.js',
    'resources/assets/js/plugins/metro.js',
    'resources/assets/js/plugins/jquery.sticky.js'
], 'public/js/plugins.js');
